# SPM Menu 1.0.2alpha

This app is in alpha stages. I will not be held responsible if your computer
explodes, leaving nothing but a mushroom cloud in its wake!


----------------------------Installation----------------------------------
# As this app is in alpha, I am only testing functionality
# It does not install itself....yet

# change directory to wherever you downloaded it to eg: 
cd ~/Downloads

# make it executable
sudo chmod +x spm-menu.py

# move it to system (so you can launch it simply by name)
sudo mv spm-menu.py /usr/sbin/spm-menu

Or if you get this from our unstable repo, you can just install it
using the SPM, which is the preferred method. Don't forget
to switch off the testing repo and re-sync afterwards!

to launch simply enter sudo spm-menu in your favorite terminal

Usage:

1. Sync repositories. 
        Updates your repo cache
2. System update
        Updates all packages, for which there are new versions
3. Install Package
        Installs a package with dependencies and reverse dependencies
4. Quick install. (no conflict check)
        Same as above but without a conflict check. Much faster
5. Remove package (with dependencies)
        Uninstall any package along with its dependencies
6. Show package description
        Shows the package's description text.
        Use before installing said package
7. List package dependencies
        Lists all dependencies of said package

#####
##### Never use Quick Install with the Unstable Repo!!!
##### It is safe with stable since those are tested
#####

If you encounter bugs. Please submit them at:
http://lsdlinux.org/bugs

Keep in mind, this is only alpha. Lots more features to come!

AUTHOR:
David Kingsbury (a.k.a. AnonKS) anonks@lsdlinux.org

Copyright ©2013 David Kingsbury. Licensed through the GNU General Public License

