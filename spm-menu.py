#!/usr/bin/env python
# spm-menu v1.0.2-alpha
# Licensed under the GNU GPLv3

from os import system
import sys
import curses

tty_colors = 0

try:
    curses.setupterm()
    tty_colors = curses.tigetnum('colors')
except:
    pass


spm_text = '''
    ######  ######  ####   ####     ####   ####  ######  ###   ##  ##    ##
    ##  ##  ##  ##  ## ## ## ##     ## ## ## ##  ######  ## #  ##  ##    ##
    ##      ##  ##  ##  ###  ##     ##  ###  ##  ##      ## #  ##  ##    ##
    ######  #####   ##       ## ### ##       ##  ####    ##  # ##  ##    ##
        ##  ##      ##       ## ### ##       ##  ##      ##  # ##  ##    ##
    ##  ##  ##      ##       ##     ##       ##  ######  ##   ###  ##    ##
    ######  ##      ##       ##     ##       ##  ######  ##    ##  ########
    ***********************************************************************
'''
def get_param(prompt_string):
    screen.clear()
    screen.border(0)
    screen.addstr(2, 2, prompt_string)
    screen.refresh()
    input = screen.getstr(10, 10, 60)
    return input

def execute_cmd(cmd_string):
    system("clear")
    a = system(cmd_string)
    print ""
    if a == 0:
        print "Command executed correctly"
    else:
        print "Command terminated with error"
    raw_input("Press enter")
    print ""

x = 0

while x != ord('8'):
    screen = curses.initscr()

    screen.clear()
    screen.border(0)
    screen.addstr(2, 6, spm_text)
    screen.addstr(12, 2, "Please enter a number...")
    screen.addstr(13, 4, "1 - Sync Repositories")
    screen.addstr(14, 4, "2 - Run a Full Update")
    screen.addstr(15, 4, "3 - Install Package")
    screen.addstr(16, 4, "4 - Fast Install (no confilct check)")
    screen.addstr(17, 4, "5 - Remove Package ")
    screen.addstr(18, 4, "6 - Show Package Description")
    screen.addstr(19, 4, "7 - List Dependencies of a Package")
    screen.addstr(21, 4, "8 - Exit")
    screen.addstr(21, 50, "spm-menu v1.0.2a")
    screen.refresh()

    x = screen.getch()
    
    # Clean and Sync Repos
    if x == ord('1'):
        curses.endwin()
        execute_cmd("spm repo -cs")

    # Clean and Sync Repos and run a System Update
    if x == ord('2'):
        curses.endwin()
        execute_cmd("spm repo -cs && spm source -aDRu world")

    # Install a package (with reverse dependencies)
    if x == ord('3'):
        pkg_inst = get_param("Enter package name, eg firefox")
        curses.endwin()
        execute_cmd("spm source -aDRu " + pkg_inst)

    # Install a package faster (i.e. no conflict check)
    # Only advisable if installing from STABLE!!
    if x == ord('4'):
        pkg_rmv = get_param("Enter package name, eg firefox")
        curses.endwin()
        execute_cmd("spm --conflicts=False source -aDRu " + fast_inst)

    # Remove a package AND its dependencies
    if x == ord('5'):
        fast_inst = get_param("Enter package name, eg firefox")
        curses.endwin()
        execute_cmd("spm source -rR " + pkg_rmv)

    # Show Package Description
    if x == ord('6'):
        list_desc = get_param("Enter package name, eg firefox")
        curses.endwin()
        execute_cmd("spm remote -d " + list_desc)

    # List a Package's Dependencies
    if x == ord('7'):
        list_deps = get_param("Enter package name, eg firefox")
        curses.endwin()
        execute_cmd("spm remote -D " + list_deps)

curses.endwin()
